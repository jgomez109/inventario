# Inventario prueba técnica
<div align="center">
<img src="https://img.shields.io/badge/CIC-Prueba%20T%C3%A9cnica-%23F0F0F0"  align="left">
</div>
<br>

# Instalación
- Clonar o descargar el repositorio.
- Ejecutar `npm install`

# Iniciar
- Ejecutar `npm run dev`
- Abrir el navegador e ir a `localhost:3000`
# Descripción
Sitio web compuesto de tres páginas: inicio, inventario de activos y agregar activo. Esta última, agregar activo, se abre desde inventario de activos.

- Página de inicio
<br>
<br>
<div align="center">
<img src="doc-repo/inicio.png" width="700px" align="center">
</div>

- Página inventario de activos
<br>
<br>
<div align="center">
<img src="doc-repo/inventario.png" width="700px" align="center">
</div>

- Página agregar activo
<br>
<br>
<div align="center">
<img src="doc-repo/agregar.png" width="700px" align="center">
</div>
