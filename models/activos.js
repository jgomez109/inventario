const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const Int32 = require('mongoose-int32');
const activosSchema = new Schema({
   id: Number,
   linea: String,
   diametro: Number,
   longitud: Number,
   fecha: Date
});

//crear Modelo 
// Mongoose automatically looks for the plural, lowercased version of your model name
// Es decir para Mascota mongoose buscara a mascotas
const Activos = mongoose.model('Activos', activosSchema);

module.exports = Activos;