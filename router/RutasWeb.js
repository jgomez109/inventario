const express = require('express');
const router = express.Router();
const Activos = require('../models/activos');


router.get('/', (req, res) => {
   res.render('index', { titulo: "¡Bienvenido!" })
})

router.get('/inventario', async (req, res) => {
   let activosDB;
   try {
      activosDB = await Activos.find()
      // console.log(activosDB);
   } catch (error) {
      console.log(error);
   }
   res.render('inventario', { titulo: "Inventario de activos", activosDB})
})

router.post('/inventario', async (req, res) => {

   const body = req.body;
   console.log(body.fecha);
   body.fecha = Date.parse(body.fecha)
   // res.redirect('/inventario')         
   try {
      await Activos.create(body);
      res.redirect('/inventario')      
   } catch (error) {
      console.log(error);
   }
})

router.get('/agregar', async (req, res) => {
   res.render('agregar', { titulo: "Agregar activo"})
})



module.exports = router;