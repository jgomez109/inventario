const path = require('path');
const bodyParser = require('body-parser');
const express = require('express'); 
const app = express();
// const port = 3000;
const port = process.env.PORT || 3000;


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

//Conexión a base de datos
const mongoose = require('mongoose');
require('dotenv').config() // Anexa las variables de .env a process.env
const uri =  `mongodb+srv://${process.env.USER}:${process.env.PASSWORD}@cluster0.tzqgp.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`;
const connect_to_mongo = async () =>{
   await mongoose.connect(uri,
      { useNewUrlParser: true, useUnifiedTopology: true } // Para evitar advertencias por consola.
   )
   .then( x => {console.log(`Conectado a la base de datos: ${x.connections[0].name}`);})
   .catch(e => console.log(e));
}

connect_to_mongo();

//Motor de plantillas (Páginas dinámicas)
app.set('view engine', 'ejs')
app.set('views', __dirname + '/views')

//Páginas estáticas
app.use(express.static(__dirname + '/public'))

//Rutas web
app.use('/', require(path.join(__dirname, './router/RutasWeb')))

app.use((req, res, next) => {
   res.status(404).render('404', { titulo: "404", descripcion: "Titulo sitio web" })
})

app.listen(port, () => {
   console.info("Express started in port", port);
})